package clases;

import java.io.Serializable;
import java.util.*;
import java.util.function.Consumer;

public class TSBArrayDequeUniqueValues<E> extends AbstractCollection<E> implements Deque<E>, Serializable, Cloneable
{
    private Object[] elements;
    private int head = 0;
    private int tail = -1;
    private int count = 0;

    public TSBArrayDequeUniqueValues(){
        this(10);
    }

    public TSBArrayDequeUniqueValues(int initialCapacity){
        if(initialCapacity <=0) initialCapacity = 10;
        this.elements = new Object[initialCapacity];
    }

    public TSBArrayDequeUniqueValues(Collection<? extends E> c ){
        elements = c.toArray();
        count = c.size();
        tail =  count - 1;
    }

    private boolean existElement(E e, boolean desc ){
        Iterator it = desc ? this.descendingIterator() : this.iterator();
        while(it.hasNext()){
            Object next = it.next();
            if(next.equals(e)) return true;
        }
        return false;
    }

    public void ensureCapacity(int minCapacity) {
        if(minCapacity == elements.length) return;
        if(minCapacity < count) return;
        Object[] temp = new Object[minCapacity];
        System.arraycopy(elements, 0, temp, 0, count);
        this.elements = temp;
        this.tail = count - 1;
    }

    @Override
    public void addFirst(E e) {
        if (e == null) {
            throw new NullPointerException();
        } else {
            if(this.existElement(e, false)) throw new IllegalArgumentException("The item already exists in the queue");
            if(count == elements.length) this.ensureCapacity(elements.length * 2);
            System.arraycopy(elements, 0, elements, 1, count);
            this.elements[0] = e;
            this.count++;
            this.tail++;
        }
    }

    @Override
    public void addLast(E e) {
        if (e == null) {
            throw new NullPointerException();
        } else {
            if(this.existElement(e, true)) throw new IllegalArgumentException("The item already exists in the queue");
            if(count == elements.length) this.ensureCapacity(elements.length * 2);
            this.elements[count] = e;
            this.count++;
            this.tail++;
        }
    }

    @Override
    public boolean offerFirst(E e) {
        try{
            this.addFirst(e);
        }catch(IllegalArgumentException ex){
            return false;
        }
        return true;
    }

    @Override
    public boolean offerLast(E e) {
        try{
            this.addLast(e);
        }catch(IllegalArgumentException ex){
            return false;
        }
        return true;
    }

    @Override
    public E removeFirst() {
        E e = this.pollFirst();
        if (e == null) {
            throw new NoSuchElementException();
        } else {
            return e;
        }
    }

    @Override
    public E removeLast() {
        E e = this.pollLast();
        if (e == null) {
            throw new NoSuchElementException();
        } else {
            return e;
        }
    }

    @Override
    public E pollFirst() {
        if(count == 0) return null;
        Object[] es;
        E e = elementAt(es = this.elements, this.head);
        if (e != null) {
            System.arraycopy(es, 1, es, 0, count-1);
            this.elements = es;
            this.count --;
            this.tail--;
        }
        return e;
    }

    @Override
    public E pollLast() {
        if(count == 0) return null;
        Object[] es;
        E e = elementAt(es = this.elements, count - 1);
        if (e != null) {
            System.arraycopy(es, 0, es, 0, count - 1 );
            this.elements = es;
            this.count --;
            this.tail --;
        }
        return e;
    }

    @Override
    public E getFirst() {
        E e = elementAt(this.elements, this.head);
        if (e == null) throw new NoSuchElementException();
        else return e;
    }

    @Override
    public E getLast(){
        if(count == 0) throw new NoSuchElementException();
        Object[] es;
        E e = elementAt(es = this.elements, tail );
        if (e == null) throw new NoSuchElementException();
        else return e;

    }
    @Override
    public E peekFirst() {
        if(count == 0) return null;
        return elementAt(this.elements, this.head);
    }

    @Override
    public E peekLast() {
        if(count == 0) return null;
        return elementAt(this.elements, tail);
    }

    @Override
    public boolean removeFirstOccurrence(Object o) {
        return removeOccurrence(o, false);
    }

    @Override
    public boolean removeLastOccurrence(Object o) {
        return removeOccurrence(o, true);
    }

    private boolean removeOccurrence(Object o, boolean last){
        if(o == null) throw new NullPointerException();
        int index = last ? tail : head;
        Iterator it = last ? this.descendingIterator() : this.iterator();
        while(it.hasNext()){
            Object next = it.next();
            if(next.equals(o)){
                delete(elements, index);
                return true;
            }
            index = last ? index-- : index ++;
        }
        return false;
    }

    private void delete(Object [] elements, int index){
        Object[] es;
        E e = elementAt(es = this.elements, index);
        if (e != null) {
            Object [] finalArray = new Object[count - 1];
            System.arraycopy(es, 0, finalArray, 0, index);
            System.arraycopy(es, index+1, finalArray, index,count-index-1 );
            this.elements = finalArray;
            this.count --;
            this.tail --;
        }
    }

    @Override
    public boolean add(E e) {
        try{
            this.addLast(e);
        } catch(IllegalArgumentException ex){
            return false;
        }
        return true;
    }

    @Override
    public boolean remove(Object o) {
        return super.remove(o);
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return super.containsAll(c);
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        return super.addAll(c);
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return super.removeAll(c);
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return super.retainAll(c);
    }

    @Override
    public void clear() {
        super.clear();
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public Iterator<E> iterator() {
        return new TSBArrayDequeUniqueValues.TSBIterator();
    }

    @Override
    public Iterator<E> descendingIterator() {
        return new TSBArrayDequeUniqueValues.TSBDescendingIterator();
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public boolean isEmpty() {
        return count == 0;
    }

    @Override
    public boolean contains(Object o) {
        return this.existElement((E) o, false);
    }

    @Override
    public Object[] toArray() {
        return super.toArray();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return super.toArray(a);
    }

    @Override
    public boolean offer(E e) {
        // Provisto por Queue y heredado por Deque... Es equivalente a offerLast()...
        return this.offerLast(e);
    }

    @Override
    public E remove() {
        // Provisto por Queue y heredado por Deque... Es equivalente a removeFirst()...
        return this.removeFirst();
    }

    @Override
    public E poll() {
        // Provisto por Queue y heredado por Deque... Es equivalente a pollFirst()...
        return this.pollFirst();
    }

    @Override
    public E element() {
        // Provisto por Queue y heredado por Deque... Es equivalente a getFirst()...
        return this.getFirst();
    }

    @Override
    public E peek() {
        // Provisto por Queue y heredado por Deque... Es equivalente a peekFirst()...
        return this.peekFirst();
    }

    @Override
    public void push(E e) {
        // Provisto por Queue y heredado por Deque... Es equivalente a addFirst(e)...
        this.addFirst(e);
    }

    @Override
    public E pop() {
        // Provisto por Queue y heredado por Deque... Es equivalente a removeFirst()...
        return this.removeFirst();
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        TSBArrayDequeUniqueValues<?> temp = (TSBArrayDequeUniqueValues<?>) super.clone();
        temp.elements = new Object[count];
        System.arraycopy(this.elements, 0, temp.elements, 0, count);
        temp.tail = count -1 ;
        return temp;
    }

    @Override
    public boolean equals(Object obj) {
        boolean ret = false;
        if(obj.getClass().equals(this.getClass()))
            ret = size() == ((TSBArrayDequeUniqueValues)obj).size();
        return ret;
    }

    @Override
    public int hashCode() {
        /*int hash = 7;
        hash = 97 * hash + count + 10;*/
        return Arrays.deepHashCode(elements);
    }

    static final <E> E nonNullElementAt(Object[] es, int i) {
        E e = (E) es[i];
        if (e == null) {
            throw new ConcurrentModificationException();
        } else {
            return e;
        }
    }

    static final <E> E elementAt(Object[] es, int i) {
        return (E) es[i];
    }

    public boolean swap(E e1, E e2){
        if(count == 0 ) return false;
        if(e1 == null || e2 == null) throw new NullPointerException();
        int [] indexElement = new int[]{-1,-1};
        int index = head;
        Iterator it = this.iterator();
        while(it.hasNext()){
            Object next = it.next();
            if(next.equals(e1)) indexElement[0] = index;
            else if(next.equals(e2)) indexElement[1] = index;
            index ++;
        }
        if(indexElement[0] != -1 && indexElement[1] != -1){
            Object [] es = this.elements;
            Object temporal = es[indexElement[0]];
            es[indexElement[0]] = es[indexElement[1]];
            es[indexElement[1]] = temporal;
            this.elements = es;
            return true;
        }
        return false;
    }

    public Iterator<E> alternatingIndexesIterator() {
        return new TSBArrayDequeUniqueValues.AlternatingIndexesIterator();
    }

    private class TSBIterator implements Iterator<E> {
        int cursor;
        int remaining = TSBArrayDequeUniqueValues.this.size();

        TSBIterator() {
            this.cursor = TSBArrayDequeUniqueValues.this.head;
        }

        public final boolean hasNext() {
            return this.remaining > 0;
        }

        public E next() {
            if (this.remaining <= 0) {
                throw new NoSuchElementException();
            } else {
                Object [] es = TSBArrayDequeUniqueValues.this.elements;
                E e = TSBArrayDequeUniqueValues.nonNullElementAt(es, this.cursor);
                this.cursor ++;
                --this.remaining;
                return e;
            }
        }

        public final void remove() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void forEachRemaining(Consumer<? super E> action) {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }

    private class TSBDescendingIterator extends TSBArrayDequeUniqueValues<E>.TSBIterator {
        TSBDescendingIterator() {
            super();
            this.cursor = tail;
        }

        public final E next() {
            if (this.remaining <= 0) {
                throw new NoSuchElementException();
            } else {
                Object[] es = TSBArrayDequeUniqueValues.this.elements;
                E e = TSBArrayDequeUniqueValues.nonNullElementAt(es, this.cursor);
                this.cursor--;
                --this.remaining;
                return e;
            }
        }
    }

    private class AlternatingIndexesIterator implements Iterator<E> {
        int cursor;
        int cursorAlternating;
        boolean runAlternating = false;
        int remaining = TSBArrayDequeUniqueValues.this.size();

        AlternatingIndexesIterator() {
            this.cursor = TSBArrayDequeUniqueValues.this.head;
            this.cursorAlternating = TSBArrayDequeUniqueValues.this.size() % 2 == 0 ? (int) Math.ceil((double)TSBArrayDequeUniqueValues.this.size() / 2) : TSBArrayDequeUniqueValues.this.size() / 2;
        }

        public final boolean hasNext() {
            return this.remaining > 0;
        }

        public E next() {
            if (this.remaining <= 0) {
                throw new NoSuchElementException();
            } else {
                Object [] es = TSBArrayDequeUniqueValues.this.elements;
                E e;
                if(!runAlternating){
                    e = TSBArrayDequeUniqueValues.nonNullElementAt(es, this.cursor);
                    this.cursor ++;
                    runAlternating = true;
                }else{
                    e = TSBArrayDequeUniqueValues.nonNullElementAt(es, this.cursorAlternating);
                    this.cursorAlternating ++;
                    runAlternating = false;
                }
                --this.remaining;
                return e;
            }
        }

        public final void remove() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void forEachRemaining(Consumer<? super E> action) {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }
}
