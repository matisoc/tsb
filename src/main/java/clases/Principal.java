package clases;

import java.util.Deque;
import java.util.Iterator;

public class Principal {
    public static void main(String[] args) {
        int data[] = {1, 5, 7, 6, 10, 2, 8, 11, 33};
        TSBArrayDequeUniqueValues deque = new TSBArrayDequeUniqueValues<Integer>();
        System.out.println("Inserting values to the deque");
        for(int value : data) {
            deque.addLast(value);
        }
        System.out.println("Deque values:" + deque.toString());
        System.out.println("Running test of swap method");
        boolean test1 = deque.swap(1,11);
        System.out.println("Result test 1 = " + test1);
        System.out.println("Deque values:" + deque.toString());
        boolean test2 = deque.swap(8,5);
        System.out.println("Result test 2 = " + test2);
        System.out.println("Deque values:" + deque.toString());
        boolean test3 = deque.swap(12,10);
        System.out.println("Result test 3 = " + test3);
        System.out.println("Deque values:" + deque.toString());
        try{
            boolean test4 = deque.swap(7, null);
        }catch(NullPointerException e){
            System.out.println("Result test 4 = failed (null values are not allowed)");
            System.out.println("Deque values:" + deque.toString());
        }
        try{
            boolean test5 = deque.swap(null, 2);
        }catch(NullPointerException e){
            System.out.println("Result test 5 = failed (null values are not allowed)");
            System.out.println("Deque values:" + deque.toString());
        }
        System.out.println("Test of iterator (alternatingIndexes)");
        Iterator it = deque.alternatingIndexesIterator();
        while(it.hasNext()){
            Object next = it.next();
            System.out.println(next);
        }
        System.out.println("Deque values:" + deque.toString());
    }
}
