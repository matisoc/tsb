package clases;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;

import java.util.Iterator;
import java.util.NoSuchElementException;

import static org.junit.Assert.*;
import static org.junit.Assert.assertNotEquals;

public class TSBArrayDequeUniqueValuesTest {

    private TSBArrayDequeUniqueValues<Integer> instance;
    private TSBArrayDequeUniqueValues<Integer> empty;

    public TSBArrayDequeUniqueValuesTest()
    {
    }

    @BeforeClass
    public static void setUpClass()
    {
    }

    @AfterClass
    public static void tearDownClass()
    {
    }

    @Before
    public void setUp()
    {
        System.out.println("* UtilsJUnit4Test: @Before method");
        int data[] = {1, 5, 7, 6, 10, 2, 8, 11};

        empty = new TSBArrayDequeUniqueValues<>();

        instance = new TSBArrayDequeUniqueValues<>();
        for(int value : data)
        {
            instance.addLast(value);
        }
    }

    @After
    public void tearDown()
    {
    }

    /**
     * Test of addFirst method, of class TSBArrayDequeUniqueValues.
     */
    @org.junit.Test (expected = Exception.class)
    public void testAddFirst()
    {
        System.out.println("addFirst");

        int ti = instance.size();
        instance.addFirst(3);
        assertEquals(ti + 1, instance.size());

        ti = instance.size();
        instance.addFirst(10);
        assertEquals(ti, instance.size());

        int data[] = {3, 1, 5, 7, 6, 10, 2, 8, 11};
        Iterator it = instance.iterator();
        for(int i=0; i<data.length; i++)
        {
            assertTrue(data[i] == (Integer)it.next());
        }

        instance.addFirst(null);
    }

    /**
     * Test of addLast method, of class TSBArrayDequeUniqueValues.
     */
    @org.junit.Test (expected = Exception.class)

    public void testAddLast()
    {
        System.out.println("addLast");

        int ti = instance.size();
        instance.addLast(3);
        assertEquals(ti + 1, instance.size());

        ti = instance.size();
        instance.addLast(10);
        assertEquals(ti, instance.size());

        int data[] = {1, 5, 7, 6, 10, 2, 8, 11, 3};
        Iterator it = instance.iterator();
        for(int i=0; i<data.length; i++)
        {
            assertTrue(data[i] == (Integer)it.next());
        }

        instance.addLast(null);
    }

    /**
     * Test of add method, of class TSBArrayDequeUniqueValues.
     */
    @org.junit.Test (expected = NullPointerException.class)
    public void testAdd()
    {
        System.out.println("add");

        int ti = instance.size();
        assertTrue(instance.add(3));
        assertFalse(instance.add(10));
        instance.add(null);
    }

    /**
     * Test of iterator method, of class TSBArrayDequeUniqueValues.
     */
    @org.junit.Test (expected = UnsupportedOperationException.class)
    public void testIterator()
    {
        System.out.println("iterator");
        Iterator r1 = instance.iterator();
        assertNotEquals(null, r1);
        assertTrue(r1.hasNext());
        assertNotEquals(null, r1.next());
        r1.remove();
    }

    /**
     * Test of descendingIterator method, of class TSBArrayDequeUniqueValues.
     */
    @org.junit.Test (expected = UnsupportedOperationException.class)
    public void testDescendingIterator()
    {
        System.out.println("descendingIterator");
        Iterator r1 = instance.descendingIterator();
        assertNotEquals(null, r1);
        assertTrue(r1.hasNext());
        assertNotEquals(null, r1.next());
        r1.remove();
    }

    /**
     * Test of AlternatingIndexesIterator method, of class TSBArrayDequeUniqueValues.
     */
    @org.junit.Test (expected = UnsupportedOperationException.class)
    public void testAlternatingIndexesIterator()
    {
        System.out.println("iterator");
        Iterator r1 = instance.alternatingIndexesIterator();
        assertNotEquals(null, r1);
        assertTrue(r1.hasNext());
        assertNotEquals(null, r1.next());
        r1.remove();
    }


    /**
     * Test of swap method, of class TSBArrayDequeUniqueValues.
     */
    @org.junit.Test (expected = NullPointerException.class)
    public void testSwap()
    {
        assertTrue(instance.swap(8,5));
        assertNull(instance.swap(1, null));
    }

    /**
     * Test of swap method, of class TSBArrayDequeUniqueValues.
     */
    @org.junit.Test
    public void testSwap2()
    {
        assertFalse(empty.swap(8,5));

    }

    /**
     * Test of size method, of class TSBArrayDequeUniqueValues.
     */
    @org.junit.Test
    public void testSize()
    {
        System.out.println("size");
        assertEquals(8, instance.size());
        assertEquals(0, empty.size());
    }

    /**
     * Test of offerFirst method, of class TSBArrayDequeUniqueValues.
     */
    @org.junit.Test (expected = NullPointerException.class)
    public void testOfferFirst()
    {
        System.out.println("offerFirst");
        assertTrue(instance.offerFirst(3));
        assertFalse(instance.offerFirst(10));
        instance.offerFirst(null);
    }

    /**
     * Test of offerLast method, of class TSBArrayDequeUniqueValues.
     */
    @org.junit.Test (expected = NullPointerException.class)
    public void testOfferLast()
    {
        System.out.println("offerFirst");
        assertTrue(instance.offerLast(3));
        assertFalse(instance.offerLast(10));
        instance.offerLast(null);
    }

    /**
     * Test of removeFirst method, of class TSBArrayDequeUniqueValues.
     */
    @org.junit.Test (expected = NoSuchElementException.class)
    public void testRemoveFirst()
    {
        System.out.println("removeFirst");
        int ta = instance.size();
        assertEquals((Integer)1, instance.removeFirst());
        assertEquals(ta - 1, instance.size());
        int r1 = empty.removeFirst();
    }

    /**
     * Test of removeLast method, of class TSBArrayDequeUniqueValues.
     */
    @org.junit.Test (expected = NoSuchElementException.class)
    public void testRemoveLast()
    {
        System.out.println("removeLast");
        int ta = instance.size();
        assertEquals((Integer)11, instance.removeLast());
        assertEquals(ta - 1, instance.size());
        int r1 = empty.removeLast();
    }

    /**
     * Test of pollFirst method, of class TSBArrayDequeUniqueValues.
     */
    @org.junit.Test
    public void testPollFirst()
    {
        System.out.println("pollFirst");
        int ta = instance.size();
        assertEquals((Integer)1, instance.pollFirst());
        assertEquals(ta - 1, instance.size());
        assertNull(empty.pollFirst());
    }

    /**
     * Test of pollLats method, of class TSBArrayDequeUniqueValues.
     */
    @org.junit.Test
    public void testPollLast()
    {
        System.out.println("pollLast");
        int ta = instance.size();
        System.out.println("size:" + ta);
        assertEquals((Integer)11, instance.pollLast());
        assertEquals(ta - 1, instance.size());
        assertNull(empty.pollLast());
    }

    /**
     * Test of getFirst method, of class TSBArrayDequeUniqueValues.
     */
    @org.junit.Test (expected = NoSuchElementException.class)
    public void testGetFirst()
    {
        System.out.println("getFirst");
        assertEquals((Integer)1, instance.getFirst());
        int r1 = empty.getFirst();
    }

    /**
     * Test of getLast method, of class TSBArrayDequeUniqueValues.
     */
    @org.junit.Test (expected = NoSuchElementException.class)
    public void testGetLast()
    {
        System.out.println("getFirst");
        assertEquals((Integer)11, instance.getLast());
        int r1 = empty.getLast();
    }

    /**
     * Test of peekFirst method, of class TSBArrayDequeUniqueValues.
     */
    @org.junit.Test
    public void testPeekFirst()
    {
        System.out.println("peekFirst");
        assertEquals((Integer)1, instance.peekFirst());
        assertNull(empty.peekFirst());
    }

    /**
     * Test of peekLast method, of class TSBArrayDequeUniqueValues.
     */
    @org.junit.Test
    public void testPeekLast()
    {
        System.out.println("peekLast");
        assertEquals((Integer)11, instance.peekLast());
        assertNull(empty.peekLast());
    }

    /**
     * Test of removeFirstOccurrence method, of class TSBArrayDequeUniqueValues.
     */
    @org.junit.Test (expected = NullPointerException.class)
    public void testRemoveFirstOccurrence()
    {
        System.out.println("removeFirstOccurrence");
        int ta = instance.size();
        assertTrue(instance.removeFirstOccurrence(7));
        assertEquals(ta - 1, instance.size());
        assertFalse(instance.removeFirstOccurrence(12));
        instance.removeFirstOccurrence(null);
    }

    /**
     * Test of removeLastOccurrence method, of class TSBArrayDequeUniqueValues.
     */
    @org.junit.Test (expected = NullPointerException.class)
    public void testRemoveLastOccurrence()
    {
        System.out.println("removeLastOccurrence");
        int ta = instance.size();
        assertTrue(instance.removeLastOccurrence(7));
        assertEquals(ta - 1, instance.size());
        assertFalse(instance.removeLastOccurrence(12));
        instance.removeLastOccurrence(null);
    }

    /**
     * Test of clone method, of class TSBArrayDequeUniqueValues.
     */
    @org.junit.Test
    public void testClone()
    {
        System.out.println("clone");
        try
        {
            TSBArrayDequeUniqueValues<Integer> copy = (TSBArrayDequeUniqueValues<Integer>)instance.clone();
            assertTrue(instance.equals(copy));
        }
        catch(CloneNotSupportedException e)
        {
        }
    }

    /**
     * Test of equals method, of class TSBArrayDequeUniqueValues.
     */
    @org.junit.Test
    public void testEquals()
    {
        System.out.println("equals");
        assertTrue(instance.equals(instance));
        assertFalse(instance.equals(empty));
    }

    /**
     * Test of hashCode method, of class TSBArrayDequeUniqueValues.
     */
    @org.junit.Test
    public void testHashCode()
    {
        System.out.println("hashCode");
        assertNotEquals(empty.hashCode(), instance.hashCode());
    }
}
